﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace MONITOR
{
    partial class Servicep : ServiceBase
    {
        private System.Timers.Timer timer = null;
        public Servicep()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            timer = new System.Timers.Timer();
            this.timer.Interval = 1000;
            this.timer.Elapsed += new ElapsedEventHandler(this.timer_tick);
            this.timer.Enabled = true;
        }

        private void timer_tick(object sender, ElapsedEventArgs e)
        {
            GetDiskValue();
            GetCpuValue();
            GetMemValue();
            Form1 f1 = new Form1();
            f1.OntimedEvent();
        }

        public int GetDiskValue()
        {
            var CpuCounter = new PerformanceCounter("PhysicalDisk", "% Disk Time", "_Total");
            CpuCounter.NextValue();
            System.Threading.Thread.Sleep(1000);
            int returnvalue = (int)CpuCounter.NextValue();
            return returnvalue;
        }

        public int GetCpuValue()
        {
            var CpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            CpuCounter.NextValue();
            System.Threading.Thread.Sleep(1000);
            int returnvalue = (int)CpuCounter.NextValue();
            return returnvalue;
        }

        public int GetMemValue()
        {
            /*PerformanceCounter mem = new PerformanceCounter("Memory", "% Committed Bytes In Use");
            float value = mem.NextValue();*/
            var MemCounter = new PerformanceCounter("Memory", "% Committed Bytes In Use");
            System.Threading.Thread.Sleep(1000);
            int returnvalue = (int)MemCounter.NextValue();

            //int value2 = Convert.ToInt32(value);
            //return value2;
            return returnvalue;
        }

        protected override void OnStop()
        {
            timer.Stop();
            timer = null;
        }
    }
}
