﻿
namespace CHESS
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.A1 = new System.Windows.Forms.Button();
            this.A2 = new System.Windows.Forms.Button();
            this.A3 = new System.Windows.Forms.Button();
            this.A4 = new System.Windows.Forms.Button();
            this.A5 = new System.Windows.Forms.Button();
            this.A6 = new System.Windows.Forms.Button();
            this.A7 = new System.Windows.Forms.Button();
            this.A8 = new System.Windows.Forms.Button();
            this.B8 = new System.Windows.Forms.Button();
            this.B7 = new System.Windows.Forms.Button();
            this.B6 = new System.Windows.Forms.Button();
            this.B5 = new System.Windows.Forms.Button();
            this.B4 = new System.Windows.Forms.Button();
            this.B3 = new System.Windows.Forms.Button();
            this.B2 = new System.Windows.Forms.Button();
            this.B1 = new System.Windows.Forms.Button();
            this.D8 = new System.Windows.Forms.Button();
            this.D7 = new System.Windows.Forms.Button();
            this.D6 = new System.Windows.Forms.Button();
            this.D5 = new System.Windows.Forms.Button();
            this.D4 = new System.Windows.Forms.Button();
            this.D3 = new System.Windows.Forms.Button();
            this.D2 = new System.Windows.Forms.Button();
            this.D1 = new System.Windows.Forms.Button();
            this.C8 = new System.Windows.Forms.Button();
            this.C7 = new System.Windows.Forms.Button();
            this.C6 = new System.Windows.Forms.Button();
            this.C5 = new System.Windows.Forms.Button();
            this.C4 = new System.Windows.Forms.Button();
            this.C3 = new System.Windows.Forms.Button();
            this.C2 = new System.Windows.Forms.Button();
            this.C1 = new System.Windows.Forms.Button();
            this.F8 = new System.Windows.Forms.Button();
            this.F7 = new System.Windows.Forms.Button();
            this.F6 = new System.Windows.Forms.Button();
            this.F5 = new System.Windows.Forms.Button();
            this.F4 = new System.Windows.Forms.Button();
            this.F3 = new System.Windows.Forms.Button();
            this.F2 = new System.Windows.Forms.Button();
            this.F1 = new System.Windows.Forms.Button();
            this.E8 = new System.Windows.Forms.Button();
            this.E7 = new System.Windows.Forms.Button();
            this.E6 = new System.Windows.Forms.Button();
            this.E5 = new System.Windows.Forms.Button();
            this.E4 = new System.Windows.Forms.Button();
            this.E3 = new System.Windows.Forms.Button();
            this.E2 = new System.Windows.Forms.Button();
            this.E1 = new System.Windows.Forms.Button();
            this.H8 = new System.Windows.Forms.Button();
            this.H7 = new System.Windows.Forms.Button();
            this.H6 = new System.Windows.Forms.Button();
            this.H5 = new System.Windows.Forms.Button();
            this.H4 = new System.Windows.Forms.Button();
            this.H3 = new System.Windows.Forms.Button();
            this.H2 = new System.Windows.Forms.Button();
            this.H1 = new System.Windows.Forms.Button();
            this.G8 = new System.Windows.Forms.Button();
            this.G7 = new System.Windows.Forms.Button();
            this.G6 = new System.Windows.Forms.Button();
            this.G5 = new System.Windows.Forms.Button();
            this.G4 = new System.Windows.Forms.Button();
            this.G3 = new System.Windows.Forms.Button();
            this.G2 = new System.Windows.Forms.Button();
            this.G1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // A1
            // 
            this.A1.BackColor = System.Drawing.Color.LightBlue;
            this.A1.FlatAppearance.BorderSize = 0;
            this.A1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.A1.Location = new System.Drawing.Point(86, 404);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(50, 50);
            this.A1.TabIndex = 0;
            this.A1.Text = " ";
            this.A1.UseVisualStyleBackColor = false;
            this.A1.Click += new System.EventHandler(this.A1_Click);
            // 
            // A2
            // 
            this.A2.BackColor = System.Drawing.Color.White;
            this.A2.FlatAppearance.BorderSize = 0;
            this.A2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.A2.Location = new System.Drawing.Point(86, 348);
            this.A2.Name = "A2";
            this.A2.Size = new System.Drawing.Size(50, 50);
            this.A2.TabIndex = 2;
            this.A2.Text = " ";
            this.A2.UseVisualStyleBackColor = false;
            this.A2.Click += new System.EventHandler(this.A2_Click);
            // 
            // A3
            // 
            this.A3.BackColor = System.Drawing.Color.LightBlue;
            this.A3.FlatAppearance.BorderSize = 0;
            this.A3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.A3.Location = new System.Drawing.Point(86, 292);
            this.A3.Name = "A3";
            this.A3.Size = new System.Drawing.Size(50, 50);
            this.A3.TabIndex = 3;
            this.A3.Text = " ";
            this.A3.UseVisualStyleBackColor = false;
            this.A3.Click += new System.EventHandler(this.A3_Click);
            // 
            // A4
            // 
            this.A4.BackColor = System.Drawing.Color.White;
            this.A4.FlatAppearance.BorderSize = 0;
            this.A4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.A4.Location = new System.Drawing.Point(86, 236);
            this.A4.Name = "A4";
            this.A4.Size = new System.Drawing.Size(50, 50);
            this.A4.TabIndex = 4;
            this.A4.Text = " ";
            this.A4.UseVisualStyleBackColor = false;
            this.A4.Click += new System.EventHandler(this.A4_Click);
            // 
            // A5
            // 
            this.A5.BackColor = System.Drawing.Color.LightBlue;
            this.A5.FlatAppearance.BorderSize = 0;
            this.A5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.A5.Location = new System.Drawing.Point(86, 180);
            this.A5.Name = "A5";
            this.A5.Size = new System.Drawing.Size(50, 50);
            this.A5.TabIndex = 5;
            this.A5.Text = " ";
            this.A5.UseVisualStyleBackColor = false;
            this.A5.Click += new System.EventHandler(this.A5_Click);
            // 
            // A6
            // 
            this.A6.BackColor = System.Drawing.Color.White;
            this.A6.FlatAppearance.BorderSize = 0;
            this.A6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.A6.Location = new System.Drawing.Point(86, 124);
            this.A6.Name = "A6";
            this.A6.Size = new System.Drawing.Size(50, 50);
            this.A6.TabIndex = 6;
            this.A6.Text = " ";
            this.A6.UseVisualStyleBackColor = false;
            this.A6.Click += new System.EventHandler(this.A6_Click);
            // 
            // A7
            // 
            this.A7.BackColor = System.Drawing.Color.LightBlue;
            this.A7.FlatAppearance.BorderSize = 0;
            this.A7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.A7.Location = new System.Drawing.Point(86, 68);
            this.A7.Name = "A7";
            this.A7.Size = new System.Drawing.Size(50, 50);
            this.A7.TabIndex = 7;
            this.A7.Text = " ";
            this.A7.UseVisualStyleBackColor = false;
            this.A7.Click += new System.EventHandler(this.A7_Click);
            // 
            // A8
            // 
            this.A8.BackColor = System.Drawing.Color.White;
            this.A8.FlatAppearance.BorderSize = 0;
            this.A8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.A8.Location = new System.Drawing.Point(86, 12);
            this.A8.Name = "A8";
            this.A8.Size = new System.Drawing.Size(50, 50);
            this.A8.TabIndex = 8;
            this.A8.Text = " ";
            this.A8.UseVisualStyleBackColor = false;
            this.A8.Click += new System.EventHandler(this.A8_Click);
            // 
            // B8
            // 
            this.B8.BackColor = System.Drawing.Color.LightBlue;
            this.B8.FlatAppearance.BorderSize = 0;
            this.B8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.B8.Location = new System.Drawing.Point(142, 12);
            this.B8.Name = "B8";
            this.B8.Size = new System.Drawing.Size(50, 50);
            this.B8.TabIndex = 16;
            this.B8.Text = " ";
            this.B8.UseVisualStyleBackColor = false;
            this.B8.Click += new System.EventHandler(this.B8_Click);
            // 
            // B7
            // 
            this.B7.BackColor = System.Drawing.Color.White;
            this.B7.FlatAppearance.BorderSize = 0;
            this.B7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.B7.Location = new System.Drawing.Point(142, 68);
            this.B7.Name = "B7";
            this.B7.Size = new System.Drawing.Size(50, 50);
            this.B7.TabIndex = 15;
            this.B7.Text = " ";
            this.B7.UseVisualStyleBackColor = false;
            this.B7.Click += new System.EventHandler(this.B7_Click);
            // 
            // B6
            // 
            this.B6.BackColor = System.Drawing.Color.LightBlue;
            this.B6.FlatAppearance.BorderSize = 0;
            this.B6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.B6.Location = new System.Drawing.Point(142, 124);
            this.B6.Name = "B6";
            this.B6.Size = new System.Drawing.Size(50, 50);
            this.B6.TabIndex = 14;
            this.B6.Text = " ";
            this.B6.UseVisualStyleBackColor = false;
            this.B6.Click += new System.EventHandler(this.B6_Click);
            // 
            // B5
            // 
            this.B5.BackColor = System.Drawing.Color.White;
            this.B5.FlatAppearance.BorderSize = 0;
            this.B5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.B5.Location = new System.Drawing.Point(142, 180);
            this.B5.Name = "B5";
            this.B5.Size = new System.Drawing.Size(50, 50);
            this.B5.TabIndex = 13;
            this.B5.Text = " ";
            this.B5.UseVisualStyleBackColor = false;
            this.B5.Click += new System.EventHandler(this.B5_Click);
            // 
            // B4
            // 
            this.B4.BackColor = System.Drawing.Color.LightBlue;
            this.B4.FlatAppearance.BorderSize = 0;
            this.B4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.B4.Location = new System.Drawing.Point(142, 236);
            this.B4.Name = "B4";
            this.B4.Size = new System.Drawing.Size(50, 50);
            this.B4.TabIndex = 12;
            this.B4.Text = " ";
            this.B4.UseVisualStyleBackColor = false;
            this.B4.Click += new System.EventHandler(this.B4_Click);
            // 
            // B3
            // 
            this.B3.BackColor = System.Drawing.Color.White;
            this.B3.FlatAppearance.BorderSize = 0;
            this.B3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.B3.Location = new System.Drawing.Point(142, 292);
            this.B3.Name = "B3";
            this.B3.Size = new System.Drawing.Size(50, 50);
            this.B3.TabIndex = 11;
            this.B3.Text = " ";
            this.B3.UseVisualStyleBackColor = false;
            this.B3.Click += new System.EventHandler(this.B3_Click);
            // 
            // B2
            // 
            this.B2.BackColor = System.Drawing.Color.LightBlue;
            this.B2.FlatAppearance.BorderSize = 0;
            this.B2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.B2.Location = new System.Drawing.Point(142, 348);
            this.B2.Name = "B2";
            this.B2.Size = new System.Drawing.Size(50, 50);
            this.B2.TabIndex = 10;
            this.B2.Text = " ";
            this.B2.UseVisualStyleBackColor = false;
            this.B2.Click += new System.EventHandler(this.B2_Click);
            // 
            // B1
            // 
            this.B1.BackColor = System.Drawing.Color.White;
            this.B1.FlatAppearance.BorderSize = 0;
            this.B1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.B1.Location = new System.Drawing.Point(142, 404);
            this.B1.Name = "B1";
            this.B1.Size = new System.Drawing.Size(50, 50);
            this.B1.TabIndex = 9;
            this.B1.Text = " ";
            this.B1.UseVisualStyleBackColor = false;
            this.B1.Click += new System.EventHandler(this.B1_Click);
            // 
            // D8
            // 
            this.D8.BackColor = System.Drawing.Color.LightBlue;
            this.D8.FlatAppearance.BorderSize = 0;
            this.D8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.D8.Location = new System.Drawing.Point(254, 12);
            this.D8.Name = "D8";
            this.D8.Size = new System.Drawing.Size(50, 50);
            this.D8.TabIndex = 32;
            this.D8.Text = " ";
            this.D8.UseVisualStyleBackColor = false;
            this.D8.Click += new System.EventHandler(this.D8_Click);
            // 
            // D7
            // 
            this.D7.BackColor = System.Drawing.Color.White;
            this.D7.FlatAppearance.BorderSize = 0;
            this.D7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.D7.Location = new System.Drawing.Point(254, 68);
            this.D7.Name = "D7";
            this.D7.Size = new System.Drawing.Size(50, 50);
            this.D7.TabIndex = 31;
            this.D7.Text = " ";
            this.D7.UseVisualStyleBackColor = false;
            this.D7.Click += new System.EventHandler(this.D7_Click);
            // 
            // D6
            // 
            this.D6.BackColor = System.Drawing.Color.LightBlue;
            this.D6.FlatAppearance.BorderSize = 0;
            this.D6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.D6.Location = new System.Drawing.Point(254, 124);
            this.D6.Name = "D6";
            this.D6.Size = new System.Drawing.Size(50, 50);
            this.D6.TabIndex = 30;
            this.D6.Text = " ";
            this.D6.UseVisualStyleBackColor = false;
            this.D6.Click += new System.EventHandler(this.D6_Click);
            // 
            // D5
            // 
            this.D5.BackColor = System.Drawing.Color.White;
            this.D5.FlatAppearance.BorderSize = 0;
            this.D5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.D5.Location = new System.Drawing.Point(254, 180);
            this.D5.Name = "D5";
            this.D5.Size = new System.Drawing.Size(50, 50);
            this.D5.TabIndex = 29;
            this.D5.Text = " ";
            this.D5.UseVisualStyleBackColor = false;
            this.D5.Click += new System.EventHandler(this.D5_Click);
            // 
            // D4
            // 
            this.D4.BackColor = System.Drawing.Color.LightBlue;
            this.D4.FlatAppearance.BorderSize = 0;
            this.D4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.D4.Location = new System.Drawing.Point(254, 236);
            this.D4.Name = "D4";
            this.D4.Size = new System.Drawing.Size(50, 50);
            this.D4.TabIndex = 28;
            this.D4.Text = " ";
            this.D4.UseVisualStyleBackColor = false;
            this.D4.Click += new System.EventHandler(this.D4_Click);
            // 
            // D3
            // 
            this.D3.BackColor = System.Drawing.Color.White;
            this.D3.FlatAppearance.BorderSize = 0;
            this.D3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.D3.Location = new System.Drawing.Point(254, 292);
            this.D3.Name = "D3";
            this.D3.Size = new System.Drawing.Size(50, 50);
            this.D3.TabIndex = 27;
            this.D3.Text = " ";
            this.D3.UseVisualStyleBackColor = false;
            this.D3.Click += new System.EventHandler(this.D3_Click);
            // 
            // D2
            // 
            this.D2.BackColor = System.Drawing.Color.LightBlue;
            this.D2.FlatAppearance.BorderSize = 0;
            this.D2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.D2.Location = new System.Drawing.Point(254, 348);
            this.D2.Name = "D2";
            this.D2.Size = new System.Drawing.Size(50, 50);
            this.D2.TabIndex = 26;
            this.D2.Text = " ";
            this.D2.UseVisualStyleBackColor = false;
            this.D2.Click += new System.EventHandler(this.D2_Click);
            // 
            // D1
            // 
            this.D1.BackColor = System.Drawing.Color.White;
            this.D1.FlatAppearance.BorderSize = 0;
            this.D1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.D1.Location = new System.Drawing.Point(254, 404);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(50, 50);
            this.D1.TabIndex = 25;
            this.D1.Text = " ";
            this.D1.UseVisualStyleBackColor = false;
            this.D1.Click += new System.EventHandler(this.D1_Click);
            // 
            // C8
            // 
            this.C8.BackColor = System.Drawing.Color.White;
            this.C8.FlatAppearance.BorderSize = 0;
            this.C8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.C8.Location = new System.Drawing.Point(198, 12);
            this.C8.Name = "C8";
            this.C8.Size = new System.Drawing.Size(50, 50);
            this.C8.TabIndex = 24;
            this.C8.Text = " ";
            this.C8.UseVisualStyleBackColor = false;
            this.C8.Click += new System.EventHandler(this.C8_Click);
            // 
            // C7
            // 
            this.C7.BackColor = System.Drawing.Color.LightBlue;
            this.C7.FlatAppearance.BorderSize = 0;
            this.C7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.C7.Location = new System.Drawing.Point(198, 68);
            this.C7.Name = "C7";
            this.C7.Size = new System.Drawing.Size(50, 50);
            this.C7.TabIndex = 23;
            this.C7.Text = " ";
            this.C7.UseVisualStyleBackColor = false;
            this.C7.Click += new System.EventHandler(this.C7_Click);
            // 
            // C6
            // 
            this.C6.BackColor = System.Drawing.Color.White;
            this.C6.FlatAppearance.BorderSize = 0;
            this.C6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.C6.Location = new System.Drawing.Point(198, 124);
            this.C6.Name = "C6";
            this.C6.Size = new System.Drawing.Size(50, 50);
            this.C6.TabIndex = 22;
            this.C6.Text = " ";
            this.C6.UseVisualStyleBackColor = false;
            this.C6.Click += new System.EventHandler(this.C6_Click);
            // 
            // C5
            // 
            this.C5.BackColor = System.Drawing.Color.LightBlue;
            this.C5.FlatAppearance.BorderSize = 0;
            this.C5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.C5.Location = new System.Drawing.Point(198, 180);
            this.C5.Name = "C5";
            this.C5.Size = new System.Drawing.Size(50, 50);
            this.C5.TabIndex = 21;
            this.C5.Text = " ";
            this.C5.UseVisualStyleBackColor = false;
            this.C5.Click += new System.EventHandler(this.C5_Click);
            // 
            // C4
            // 
            this.C4.BackColor = System.Drawing.Color.White;
            this.C4.FlatAppearance.BorderSize = 0;
            this.C4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.C4.Location = new System.Drawing.Point(198, 236);
            this.C4.Name = "C4";
            this.C4.Size = new System.Drawing.Size(50, 50);
            this.C4.TabIndex = 20;
            this.C4.Text = " ";
            this.C4.UseVisualStyleBackColor = false;
            this.C4.Click += new System.EventHandler(this.C4_Click);
            // 
            // C3
            // 
            this.C3.BackColor = System.Drawing.Color.LightBlue;
            this.C3.FlatAppearance.BorderSize = 0;
            this.C3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.C3.Location = new System.Drawing.Point(198, 292);
            this.C3.Name = "C3";
            this.C3.Size = new System.Drawing.Size(50, 50);
            this.C3.TabIndex = 19;
            this.C3.Text = " ";
            this.C3.UseVisualStyleBackColor = false;
            this.C3.Click += new System.EventHandler(this.C3_Click);
            // 
            // C2
            // 
            this.C2.BackColor = System.Drawing.Color.White;
            this.C2.FlatAppearance.BorderSize = 0;
            this.C2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.C2.Location = new System.Drawing.Point(198, 348);
            this.C2.Name = "C2";
            this.C2.Size = new System.Drawing.Size(50, 50);
            this.C2.TabIndex = 18;
            this.C2.Text = " ";
            this.C2.UseVisualStyleBackColor = false;
            this.C2.Click += new System.EventHandler(this.C2_Click);
            // 
            // C1
            // 
            this.C1.BackColor = System.Drawing.Color.LightBlue;
            this.C1.FlatAppearance.BorderSize = 0;
            this.C1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.C1.Location = new System.Drawing.Point(198, 404);
            this.C1.Name = "C1";
            this.C1.Size = new System.Drawing.Size(50, 50);
            this.C1.TabIndex = 17;
            this.C1.Text = " ";
            this.C1.UseVisualStyleBackColor = false;
            this.C1.Click += new System.EventHandler(this.C1_Click);
            // 
            // F8
            // 
            this.F8.BackColor = System.Drawing.Color.LightBlue;
            this.F8.FlatAppearance.BorderSize = 0;
            this.F8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.F8.Location = new System.Drawing.Point(366, 12);
            this.F8.Name = "F8";
            this.F8.Size = new System.Drawing.Size(50, 50);
            this.F8.TabIndex = 48;
            this.F8.Text = " ";
            this.F8.UseVisualStyleBackColor = false;
            this.F8.Click += new System.EventHandler(this.F8_Click);
            // 
            // F7
            // 
            this.F7.BackColor = System.Drawing.Color.White;
            this.F7.FlatAppearance.BorderSize = 0;
            this.F7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.F7.Location = new System.Drawing.Point(366, 68);
            this.F7.Name = "F7";
            this.F7.Size = new System.Drawing.Size(50, 50);
            this.F7.TabIndex = 47;
            this.F7.Text = " ";
            this.F7.UseVisualStyleBackColor = false;
            this.F7.Click += new System.EventHandler(this.F7_Click);
            // 
            // F6
            // 
            this.F6.BackColor = System.Drawing.Color.LightBlue;
            this.F6.FlatAppearance.BorderSize = 0;
            this.F6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.F6.Location = new System.Drawing.Point(366, 124);
            this.F6.Name = "F6";
            this.F6.Size = new System.Drawing.Size(50, 50);
            this.F6.TabIndex = 46;
            this.F6.Text = " ";
            this.F6.UseVisualStyleBackColor = false;
            this.F6.Click += new System.EventHandler(this.F6_Click);
            // 
            // F5
            // 
            this.F5.BackColor = System.Drawing.Color.White;
            this.F5.FlatAppearance.BorderSize = 0;
            this.F5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.F5.Location = new System.Drawing.Point(366, 180);
            this.F5.Name = "F5";
            this.F5.Size = new System.Drawing.Size(50, 50);
            this.F5.TabIndex = 45;
            this.F5.Text = " ";
            this.F5.UseVisualStyleBackColor = false;
            this.F5.Click += new System.EventHandler(this.F5_Click);
            // 
            // F4
            // 
            this.F4.BackColor = System.Drawing.Color.LightBlue;
            this.F4.FlatAppearance.BorderSize = 0;
            this.F4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.F4.Location = new System.Drawing.Point(366, 236);
            this.F4.Name = "F4";
            this.F4.Size = new System.Drawing.Size(50, 50);
            this.F4.TabIndex = 44;
            this.F4.Text = " ";
            this.F4.UseVisualStyleBackColor = false;
            this.F4.Click += new System.EventHandler(this.F4_Click);
            // 
            // F3
            // 
            this.F3.BackColor = System.Drawing.Color.White;
            this.F3.FlatAppearance.BorderSize = 0;
            this.F3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.F3.Location = new System.Drawing.Point(366, 292);
            this.F3.Name = "F3";
            this.F3.Size = new System.Drawing.Size(50, 50);
            this.F3.TabIndex = 43;
            this.F3.Text = " ";
            this.F3.UseVisualStyleBackColor = false;
            this.F3.Click += new System.EventHandler(this.F3_Click);
            // 
            // F2
            // 
            this.F2.BackColor = System.Drawing.Color.LightBlue;
            this.F2.FlatAppearance.BorderSize = 0;
            this.F2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.F2.Location = new System.Drawing.Point(366, 348);
            this.F2.Name = "F2";
            this.F2.Size = new System.Drawing.Size(50, 50);
            this.F2.TabIndex = 42;
            this.F2.Text = " ";
            this.F2.UseVisualStyleBackColor = false;
            this.F2.Click += new System.EventHandler(this.F2_Click);
            // 
            // F1
            // 
            this.F1.BackColor = System.Drawing.Color.White;
            this.F1.FlatAppearance.BorderSize = 0;
            this.F1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.F1.Location = new System.Drawing.Point(366, 404);
            this.F1.Name = "F1";
            this.F1.Size = new System.Drawing.Size(50, 50);
            this.F1.TabIndex = 41;
            this.F1.Text = " ";
            this.F1.UseVisualStyleBackColor = false;
            this.F1.Click += new System.EventHandler(this.F1_Click);
            // 
            // E8
            // 
            this.E8.BackColor = System.Drawing.Color.White;
            this.E8.FlatAppearance.BorderSize = 0;
            this.E8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.E8.Location = new System.Drawing.Point(310, 12);
            this.E8.Name = "E8";
            this.E8.Size = new System.Drawing.Size(50, 50);
            this.E8.TabIndex = 40;
            this.E8.Text = " ";
            this.E8.UseVisualStyleBackColor = false;
            this.E8.Click += new System.EventHandler(this.E8_Click);
            // 
            // E7
            // 
            this.E7.BackColor = System.Drawing.Color.LightBlue;
            this.E7.FlatAppearance.BorderSize = 0;
            this.E7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.E7.Location = new System.Drawing.Point(310, 68);
            this.E7.Name = "E7";
            this.E7.Size = new System.Drawing.Size(50, 50);
            this.E7.TabIndex = 39;
            this.E7.Text = " ";
            this.E7.UseVisualStyleBackColor = false;
            this.E7.Click += new System.EventHandler(this.E7_Click);
            // 
            // E6
            // 
            this.E6.BackColor = System.Drawing.Color.White;
            this.E6.FlatAppearance.BorderSize = 0;
            this.E6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.E6.Location = new System.Drawing.Point(310, 124);
            this.E6.Name = "E6";
            this.E6.Size = new System.Drawing.Size(50, 50);
            this.E6.TabIndex = 38;
            this.E6.Text = " ";
            this.E6.UseVisualStyleBackColor = false;
            this.E6.Click += new System.EventHandler(this.E6_Click);
            // 
            // E5
            // 
            this.E5.BackColor = System.Drawing.Color.LightBlue;
            this.E5.FlatAppearance.BorderSize = 0;
            this.E5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.E5.Location = new System.Drawing.Point(310, 180);
            this.E5.Name = "E5";
            this.E5.Size = new System.Drawing.Size(50, 50);
            this.E5.TabIndex = 37;
            this.E5.Text = " ";
            this.E5.UseVisualStyleBackColor = false;
            this.E5.Click += new System.EventHandler(this.E5_Click);
            // 
            // E4
            // 
            this.E4.BackColor = System.Drawing.Color.White;
            this.E4.FlatAppearance.BorderSize = 0;
            this.E4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.E4.Location = new System.Drawing.Point(310, 236);
            this.E4.Name = "E4";
            this.E4.Size = new System.Drawing.Size(50, 50);
            this.E4.TabIndex = 36;
            this.E4.Text = " ";
            this.E4.UseVisualStyleBackColor = false;
            this.E4.Click += new System.EventHandler(this.E4_Click);
            // 
            // E3
            // 
            this.E3.BackColor = System.Drawing.Color.LightBlue;
            this.E3.FlatAppearance.BorderSize = 0;
            this.E3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.E3.Location = new System.Drawing.Point(310, 292);
            this.E3.Name = "E3";
            this.E3.Size = new System.Drawing.Size(50, 50);
            this.E3.TabIndex = 35;
            this.E3.Text = " ";
            this.E3.UseVisualStyleBackColor = false;
            this.E3.Click += new System.EventHandler(this.E3_Click);
            // 
            // E2
            // 
            this.E2.BackColor = System.Drawing.Color.White;
            this.E2.FlatAppearance.BorderSize = 0;
            this.E2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.E2.Location = new System.Drawing.Point(310, 348);
            this.E2.Name = "E2";
            this.E2.Size = new System.Drawing.Size(50, 50);
            this.E2.TabIndex = 34;
            this.E2.Text = " ";
            this.E2.UseVisualStyleBackColor = false;
            this.E2.Click += new System.EventHandler(this.E2_Click);
            // 
            // E1
            // 
            this.E1.BackColor = System.Drawing.Color.LightBlue;
            this.E1.FlatAppearance.BorderSize = 0;
            this.E1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.E1.Location = new System.Drawing.Point(310, 404);
            this.E1.Name = "E1";
            this.E1.Size = new System.Drawing.Size(50, 50);
            this.E1.TabIndex = 33;
            this.E1.Text = " ";
            this.E1.UseVisualStyleBackColor = false;
            this.E1.Click += new System.EventHandler(this.E1_Click);
            // 
            // H8
            // 
            this.H8.BackColor = System.Drawing.Color.LightBlue;
            this.H8.FlatAppearance.BorderSize = 0;
            this.H8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.H8.Location = new System.Drawing.Point(478, 12);
            this.H8.Name = "H8";
            this.H8.Size = new System.Drawing.Size(50, 50);
            this.H8.TabIndex = 64;
            this.H8.Text = " ";
            this.H8.UseVisualStyleBackColor = false;
            this.H8.Click += new System.EventHandler(this.H8_Click);
            // 
            // H7
            // 
            this.H7.BackColor = System.Drawing.Color.White;
            this.H7.FlatAppearance.BorderSize = 0;
            this.H7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.H7.Location = new System.Drawing.Point(478, 68);
            this.H7.Name = "H7";
            this.H7.Size = new System.Drawing.Size(50, 50);
            this.H7.TabIndex = 63;
            this.H7.Text = " ";
            this.H7.UseVisualStyleBackColor = false;
            this.H7.Click += new System.EventHandler(this.H7_Click);
            // 
            // H6
            // 
            this.H6.BackColor = System.Drawing.Color.LightBlue;
            this.H6.FlatAppearance.BorderSize = 0;
            this.H6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.H6.Location = new System.Drawing.Point(478, 124);
            this.H6.Name = "H6";
            this.H6.Size = new System.Drawing.Size(50, 50);
            this.H6.TabIndex = 62;
            this.H6.Text = " ";
            this.H6.UseVisualStyleBackColor = false;
            this.H6.Click += new System.EventHandler(this.H6_Click);
            // 
            // H5
            // 
            this.H5.BackColor = System.Drawing.Color.White;
            this.H5.FlatAppearance.BorderSize = 0;
            this.H5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.H5.Location = new System.Drawing.Point(478, 180);
            this.H5.Name = "H5";
            this.H5.Size = new System.Drawing.Size(50, 50);
            this.H5.TabIndex = 61;
            this.H5.Text = " ";
            this.H5.UseVisualStyleBackColor = false;
            this.H5.Click += new System.EventHandler(this.H5_Click);
            // 
            // H4
            // 
            this.H4.BackColor = System.Drawing.Color.LightBlue;
            this.H4.FlatAppearance.BorderSize = 0;
            this.H4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.H4.Location = new System.Drawing.Point(478, 236);
            this.H4.Name = "H4";
            this.H4.Size = new System.Drawing.Size(50, 50);
            this.H4.TabIndex = 60;
            this.H4.Text = " ";
            this.H4.UseVisualStyleBackColor = false;
            this.H4.Click += new System.EventHandler(this.H4_Click);
            // 
            // H3
            // 
            this.H3.BackColor = System.Drawing.Color.White;
            this.H3.FlatAppearance.BorderSize = 0;
            this.H3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.H3.Location = new System.Drawing.Point(478, 292);
            this.H3.Name = "H3";
            this.H3.Size = new System.Drawing.Size(50, 50);
            this.H3.TabIndex = 59;
            this.H3.Text = " ";
            this.H3.UseVisualStyleBackColor = false;
            this.H3.Click += new System.EventHandler(this.H3_Click);
            // 
            // H2
            // 
            this.H2.BackColor = System.Drawing.Color.LightBlue;
            this.H2.FlatAppearance.BorderSize = 0;
            this.H2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.H2.Location = new System.Drawing.Point(478, 348);
            this.H2.Name = "H2";
            this.H2.Size = new System.Drawing.Size(50, 50);
            this.H2.TabIndex = 58;
            this.H2.Text = " ";
            this.H2.UseVisualStyleBackColor = false;
            this.H2.Click += new System.EventHandler(this.H2_Click);
            // 
            // H1
            // 
            this.H1.BackColor = System.Drawing.Color.White;
            this.H1.FlatAppearance.BorderSize = 0;
            this.H1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.H1.Location = new System.Drawing.Point(478, 404);
            this.H1.Name = "H1";
            this.H1.Size = new System.Drawing.Size(50, 50);
            this.H1.TabIndex = 57;
            this.H1.Text = " ";
            this.H1.UseVisualStyleBackColor = false;
            this.H1.Click += new System.EventHandler(this.H1_Click);
            // 
            // G8
            // 
            this.G8.BackColor = System.Drawing.Color.White;
            this.G8.FlatAppearance.BorderSize = 0;
            this.G8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.G8.Location = new System.Drawing.Point(422, 12);
            this.G8.Name = "G8";
            this.G8.Size = new System.Drawing.Size(50, 50);
            this.G8.TabIndex = 56;
            this.G8.Text = " ";
            this.G8.UseVisualStyleBackColor = false;
            this.G8.Click += new System.EventHandler(this.G8_Click);
            // 
            // G7
            // 
            this.G7.BackColor = System.Drawing.Color.LightBlue;
            this.G7.FlatAppearance.BorderSize = 0;
            this.G7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.G7.Location = new System.Drawing.Point(422, 68);
            this.G7.Name = "G7";
            this.G7.Size = new System.Drawing.Size(50, 50);
            this.G7.TabIndex = 55;
            this.G7.Text = " ";
            this.G7.UseVisualStyleBackColor = false;
            this.G7.Click += new System.EventHandler(this.G7_Click);
            // 
            // G6
            // 
            this.G6.BackColor = System.Drawing.Color.White;
            this.G6.FlatAppearance.BorderSize = 0;
            this.G6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.G6.Location = new System.Drawing.Point(422, 124);
            this.G6.Name = "G6";
            this.G6.Size = new System.Drawing.Size(50, 50);
            this.G6.TabIndex = 54;
            this.G6.Text = " ";
            this.G6.UseVisualStyleBackColor = false;
            this.G6.Click += new System.EventHandler(this.G6_Click);
            // 
            // G5
            // 
            this.G5.BackColor = System.Drawing.Color.LightBlue;
            this.G5.FlatAppearance.BorderSize = 0;
            this.G5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.G5.Location = new System.Drawing.Point(422, 180);
            this.G5.Name = "G5";
            this.G5.Size = new System.Drawing.Size(50, 50);
            this.G5.TabIndex = 53;
            this.G5.Text = " ";
            this.G5.UseVisualStyleBackColor = false;
            this.G5.Click += new System.EventHandler(this.G5_Click);
            // 
            // G4
            // 
            this.G4.BackColor = System.Drawing.Color.White;
            this.G4.FlatAppearance.BorderSize = 0;
            this.G4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.G4.Location = new System.Drawing.Point(422, 236);
            this.G4.Name = "G4";
            this.G4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.G4.Size = new System.Drawing.Size(50, 50);
            this.G4.TabIndex = 52;
            this.G4.Text = " ";
            this.G4.UseVisualStyleBackColor = false;
            this.G4.Click += new System.EventHandler(this.G4_Click);
            // 
            // G3
            // 
            this.G3.BackColor = System.Drawing.Color.LightBlue;
            this.G3.FlatAppearance.BorderSize = 0;
            this.G3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.G3.Location = new System.Drawing.Point(422, 292);
            this.G3.Name = "G3";
            this.G3.Size = new System.Drawing.Size(50, 50);
            this.G3.TabIndex = 51;
            this.G3.Text = " ";
            this.G3.UseVisualStyleBackColor = false;
            this.G3.Click += new System.EventHandler(this.G3_Click);
            // 
            // G2
            // 
            this.G2.BackColor = System.Drawing.Color.White;
            this.G2.FlatAppearance.BorderSize = 0;
            this.G2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.G2.Location = new System.Drawing.Point(422, 348);
            this.G2.Name = "G2";
            this.G2.Size = new System.Drawing.Size(50, 50);
            this.G2.TabIndex = 50;
            this.G2.Text = " ";
            this.G2.UseVisualStyleBackColor = false;
            this.G2.Click += new System.EventHandler(this.G2_Click);
            // 
            // G1
            // 
            this.G1.BackColor = System.Drawing.Color.LightBlue;
            this.G1.FlatAppearance.BorderSize = 0;
            this.G1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.G1.Location = new System.Drawing.Point(422, 404);
            this.G1.Name = "G1";
            this.G1.Size = new System.Drawing.Size(50, 50);
            this.G1.TabIndex = 49;
            this.G1.Text = " ";
            this.G1.UseVisualStyleBackColor = false;
            this.G1.Click += new System.EventHandler(this.G1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 466);
            this.Controls.Add(this.H8);
            this.Controls.Add(this.H7);
            this.Controls.Add(this.H6);
            this.Controls.Add(this.H5);
            this.Controls.Add(this.H4);
            this.Controls.Add(this.H3);
            this.Controls.Add(this.H2);
            this.Controls.Add(this.H1);
            this.Controls.Add(this.G8);
            this.Controls.Add(this.G7);
            this.Controls.Add(this.G6);
            this.Controls.Add(this.G5);
            this.Controls.Add(this.G4);
            this.Controls.Add(this.G3);
            this.Controls.Add(this.G2);
            this.Controls.Add(this.G1);
            this.Controls.Add(this.F8);
            this.Controls.Add(this.F7);
            this.Controls.Add(this.F6);
            this.Controls.Add(this.F5);
            this.Controls.Add(this.F4);
            this.Controls.Add(this.F3);
            this.Controls.Add(this.F2);
            this.Controls.Add(this.F1);
            this.Controls.Add(this.E8);
            this.Controls.Add(this.E7);
            this.Controls.Add(this.E6);
            this.Controls.Add(this.E5);
            this.Controls.Add(this.E4);
            this.Controls.Add(this.E3);
            this.Controls.Add(this.E2);
            this.Controls.Add(this.E1);
            this.Controls.Add(this.D8);
            this.Controls.Add(this.D7);
            this.Controls.Add(this.D6);
            this.Controls.Add(this.D5);
            this.Controls.Add(this.D4);
            this.Controls.Add(this.D3);
            this.Controls.Add(this.D2);
            this.Controls.Add(this.D1);
            this.Controls.Add(this.C8);
            this.Controls.Add(this.C7);
            this.Controls.Add(this.C6);
            this.Controls.Add(this.C5);
            this.Controls.Add(this.C4);
            this.Controls.Add(this.C3);
            this.Controls.Add(this.C2);
            this.Controls.Add(this.C1);
            this.Controls.Add(this.B8);
            this.Controls.Add(this.B7);
            this.Controls.Add(this.B6);
            this.Controls.Add(this.B5);
            this.Controls.Add(this.B4);
            this.Controls.Add(this.B3);
            this.Controls.Add(this.B2);
            this.Controls.Add(this.B1);
            this.Controls.Add(this.A8);
            this.Controls.Add(this.A7);
            this.Controls.Add(this.A6);
            this.Controls.Add(this.A5);
            this.Controls.Add(this.A4);
            this.Controls.Add(this.A3);
            this.Controls.Add(this.A2);
            this.Controls.Add(this.A1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button A1;
        private System.Windows.Forms.Button A2;
        private System.Windows.Forms.Button A3;
        private System.Windows.Forms.Button A4;
        private System.Windows.Forms.Button A5;
        private System.Windows.Forms.Button A6;
        private System.Windows.Forms.Button A7;
        private System.Windows.Forms.Button A8;
        private System.Windows.Forms.Button B8;
        private System.Windows.Forms.Button B7;
        private System.Windows.Forms.Button B6;
        private System.Windows.Forms.Button B5;
        private System.Windows.Forms.Button B4;
        private System.Windows.Forms.Button B3;
        private System.Windows.Forms.Button B2;
        private System.Windows.Forms.Button B1;
        private System.Windows.Forms.Button D8;
        private System.Windows.Forms.Button D7;
        private System.Windows.Forms.Button D6;
        private System.Windows.Forms.Button D5;
        private System.Windows.Forms.Button D4;
        private System.Windows.Forms.Button D3;
        private System.Windows.Forms.Button D2;
        private System.Windows.Forms.Button D1;
        private System.Windows.Forms.Button C8;
        private System.Windows.Forms.Button C7;
        private System.Windows.Forms.Button C6;
        private System.Windows.Forms.Button C5;
        private System.Windows.Forms.Button C4;
        private System.Windows.Forms.Button C3;
        private System.Windows.Forms.Button C2;
        private System.Windows.Forms.Button C1;
        private System.Windows.Forms.Button F8;
        private System.Windows.Forms.Button F7;
        private System.Windows.Forms.Button F6;
        private System.Windows.Forms.Button F5;
        private System.Windows.Forms.Button F4;
        private System.Windows.Forms.Button F3;
        private System.Windows.Forms.Button F2;
        private System.Windows.Forms.Button F1;
        private System.Windows.Forms.Button E8;
        private System.Windows.Forms.Button E7;
        private System.Windows.Forms.Button E6;
        private System.Windows.Forms.Button E5;
        private System.Windows.Forms.Button E4;
        private System.Windows.Forms.Button E3;
        private System.Windows.Forms.Button E2;
        private System.Windows.Forms.Button E1;
        private System.Windows.Forms.Button H8;
        private System.Windows.Forms.Button H7;
        private System.Windows.Forms.Button H6;
        private System.Windows.Forms.Button H5;
        private System.Windows.Forms.Button H4;
        private System.Windows.Forms.Button H3;
        private System.Windows.Forms.Button H2;
        private System.Windows.Forms.Button H1;
        private System.Windows.Forms.Button G8;
        private System.Windows.Forms.Button G7;
        private System.Windows.Forms.Button G6;
        private System.Windows.Forms.Button G5;
        private System.Windows.Forms.Button G4;
        private System.Windows.Forms.Button G3;
        private System.Windows.Forms.Button G2;
        private System.Windows.Forms.Button G1;
    }
}

