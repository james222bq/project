﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace CHAD
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Socket sck;
        private void Form1_Load(object sender, EventArgs e)
        {
            sck = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sck.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

            textLocalIp.Text = GetLocalIP();
            textFriendIp.Text = GetLocalIP();
        }
            IPHostEntry host;
        private string GetLocalIP()
        {
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach(IPAddress ip in host.AddressList)
            {
                if(ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "127.0.0.1";
        }
        
        EndPoint epRemote;
        IPEndPoint epLocal;

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                epLocal = new IPEndPoint(IPAddress.Parse(textLocalIp.Text),
                    Convert.ToInt32(textLocalPort.Text));
                sck.Bind(epLocal);

                epRemote = new IPEndPoint(IPAddress.Parse(textFriendIp.Text),
                    Convert.ToInt32(textFriendsPort.Text));
                sck.Connect(epRemote);

                Byte[] buffer = new byte[1500];
                sck.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref epRemote,
                    new AsyncCallback(MessageCallBack), buffer);

                buttonSend.Enabled = true;
                button1.Text = "Connected";
                button1.Enabled = false;
                textMessage.Focus();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }           
        }

        private void MessageCallBack(IAsyncResult aResult)
        {
            try
            {
                int size = sck.EndReceiveFrom(aResult, ref epRemote);
                byte[] receivedData = new byte[1464];
                receivedData = (byte[])aResult.AsyncState;

                ASCIIEncoding eEncoding = new ASCIIEncoding();
                string receivedMessage = eEncoding.GetString(receivedData);

                listMessage.Items.Add("Friend: " + receivedMessage);

                byte[] buffer = new byte[1500];
                sck.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None,
                    ref epRemote, new AsyncCallback(MessageCallBack), buffer);
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            try
            {
                System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
                byte[] msg = new byte[1500];
                msg = enc.GetBytes(textMessage.Text);
                sck.Send(msg);
                listMessage.Items.Add("You: " + textMessage.Text);
                textMessage.Clear();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool ruchx = true;
        public bool ruchy = false;
        public void tictactoe()
        {
            if((topleft.Text == "O") && (top.Text == "O") && (topright.Text == "O"))
            {
                MessageBox.Show("WYGRANA O");
            }
            if ((topleft.Text == "X") && (top.Text == "X") && (topright.Text == "X"))
            {
                MessageBox.Show("WYGRANA X");
            }
            if ((left.Text == "O") && (center.Text == "O") && (right.Text == "O"))
            {
                MessageBox.Show("WYGRANA O");
            }
            if ((left.Text == "X") && (center.Text == "X") && (right.Text == "X"))
            {
                MessageBox.Show("WYGRANA X");
            }
            if ((bottomleft.Text == "O") && (bottom.Text == "O") && (bottomright.Text == "O"))
            {
                MessageBox.Show("WYGRANA O");
            }
            if ((bottomleft.Text == "X") && (bottom.Text == "X") && (bottomright.Text == "X"))
            {
                MessageBox.Show("WYGRANA X");
            }
            /*============================================*/
            if ((topleft.Text == "O") && (left.Text == "O") && (bottom.Text == "O"))
            {
                MessageBox.Show("WYGRANA O");
            }
            if ((topleft.Text == "X") && (left.Text == "X") && (bottom.Text == "X"))
            {
                MessageBox.Show("WYGRANA X");
            }
            if ((top.Text == "O") && (center.Text == "O") && (bottom.Text == "O"))
            {
                MessageBox.Show("WYGRANA O");
            }
            if ((top.Text == "X") && (center.Text == "X") && (bottom.Text == "X"))
            {
                MessageBox.Show("WYGRANA X");
            }
            if ((topright.Text == "O") && (right.Text == "O") && (bottomright.Text == "O"))
            {
                MessageBox.Show("WYGRANA O");
            }
            if ((topright.Text == "X") && (right.Text == "X") && (bottomright.Text == "X"))
            {
                MessageBox.Show("WYGRANA X");
            }

            if ((topleft.Text == "O") && (center.Text == "O") && (bottomright.Text == "O"))
            {
                MessageBox.Show("WYGRANA O");
            }
            if ((topleft.Text == "X") && (center.Text == "X") && (bottomright.Text == "X"))
            {
                MessageBox.Show("WYGRANA X");
            }

            if ((topright.Text == "O") && (center.Text == "O") && (bottomleft.Text == "O"))
            {
                MessageBox.Show("WYGRANA O");
            }
            if ((topright.Text == "X") && (center.Text == "X") && (bottomleft.Text == "X"))
            {
                MessageBox.Show("WYGRANA X");
            }
        }

        private void topleft_Click(object sender, EventArgs e)
        {
            if (ruchx == true)
            {
                topleft.Text = "O";
                ruchx = false;
            }
            else
            {
                topleft.Text = "X";
                ruchx = true;
            }
            topleft.Enabled = false;
            tictactoe();
        }

        private void top_Click(object sender, EventArgs e)
        {
            if (ruchx == true)
            {
                top.Text = "O";
                ruchx = false;
            }
            else
            {
                top.Text = "X";
                ruchx = true;
            }
            top.Enabled = false;
            tictactoe();
        }

        private void topright_Click(object sender, EventArgs e)
        {
            if (ruchx == true)
            {
                topright.Text = "O";
                ruchx = false;
            }
            else
            {
                topright.Text = "X";
                ruchx = true;
            }
            topright.Enabled = false;
            tictactoe();
        }

        private void left_Click(object sender, EventArgs e)
        {
            if (ruchx == true)
            {
                left.Text = "O";
                ruchx = false;
            }
            else
            {
                left.Text = "X";
                ruchx = true;
            }
            left.Enabled = false;
            tictactoe();
        }

        private void center_Click(object sender, EventArgs e)
        {
            if (ruchx == true)
            {
                center.Text = "O";
                ruchx = false;
            }
            else
            {
                center.Text = "X";
                ruchx = true;
            }
            center.Enabled = false;
            tictactoe();
        }

        private void right_Click(object sender, EventArgs e)
        {
            if (ruchx == true)
            {
                right.Text = "O";
                ruchx = false;
            }
            else
            {
                right.Text = "X";
                ruchx = true;
            }
            right.Enabled = false;
            tictactoe();
        }

        private void bottomleft_Click(object sender, EventArgs e)
        {
            if (ruchx == true)
            {
                bottomleft.Text = "O";
                ruchx = false;
            }
            else
            {
                bottomleft.Text = "X";
                ruchx = true;
            }
            bottomleft.Enabled = false;
            tictactoe();
        }

        private void bottom_Click(object sender, EventArgs e)
        {
            if (ruchx == true)
            {
                bottom.Text = "O";
                ruchx = false;
            }
            else
            {
                bottom.Text = "X";
                ruchx = true;
            }
            bottom.Enabled = false;
            tictactoe();
        }

        private void bottomright_Click(object sender, EventArgs e)
        {
            if (ruchx == true)
            {
                bottomright.Text = "O";
                ruchx = false;
            }
            else
            {
                bottomright.Text = "X";
                ruchx = true;
            }
            bottomright.Enabled = false;
            tictactoe();
        }
    }
}
