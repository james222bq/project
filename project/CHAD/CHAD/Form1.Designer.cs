﻿
namespace CHAD
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.textLocalIp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textLocalPort = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textFriendIp = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textFriendsPort = new System.Windows.Forms.TextBox();
            this.listMessage = new System.Windows.Forms.ListBox();
            this.textMessage = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonSend = new System.Windows.Forms.Button();
            this.topleft = new System.Windows.Forms.Button();
            this.top = new System.Windows.Forms.Button();
            this.topright = new System.Windows.Forms.Button();
            this.left = new System.Windows.Forms.Button();
            this.center = new System.Windows.Forms.Button();
            this.right = new System.Windows.Forms.Button();
            this.bottomleft = new System.Windows.Forms.Button();
            this.bottom = new System.Windows.Forms.Button();
            this.bottomright = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textLocalIp
            // 
            this.textLocalIp.BackColor = System.Drawing.Color.Magenta;
            this.textLocalIp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textLocalIp.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textLocalIp.ForeColor = System.Drawing.Color.White;
            this.textLocalIp.Location = new System.Drawing.Point(206, 9);
            this.textLocalIp.Name = "textLocalIp";
            this.textLocalIp.Size = new System.Drawing.Size(203, 40);
            this.textLocalIp.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(68, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 39);
            this.label1.TabIndex = 1;
            this.label1.Text = "Your IP:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(480, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 39);
            this.label2.TabIndex = 3;
            this.label2.Text = "Your Port:";
            // 
            // textLocalPort
            // 
            this.textLocalPort.BackColor = System.Drawing.Color.Magenta;
            this.textLocalPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textLocalPort.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textLocalPort.ForeColor = System.Drawing.Color.White;
            this.textLocalPort.Location = new System.Drawing.Point(644, 8);
            this.textLocalPort.Name = "textLocalPort";
            this.textLocalPort.Size = new System.Drawing.Size(121, 40);
            this.textLocalPort.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(18, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(182, 39);
            this.label3.TabIndex = 5;
            this.label3.Text = "Friend\'s IP:";
            // 
            // textFriendIp
            // 
            this.textFriendIp.BackColor = System.Drawing.Color.Magenta;
            this.textFriendIp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textFriendIp.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textFriendIp.ForeColor = System.Drawing.Color.White;
            this.textFriendIp.Location = new System.Drawing.Point(206, 79);
            this.textFriendIp.Name = "textFriendIp";
            this.textFriendIp.Size = new System.Drawing.Size(203, 40);
            this.textFriendIp.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(422, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(208, 39);
            this.label4.TabIndex = 7;
            this.label4.Text = "Friend\'s Port:";
            // 
            // textFriendsPort
            // 
            this.textFriendsPort.BackColor = System.Drawing.Color.Magenta;
            this.textFriendsPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textFriendsPort.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textFriendsPort.ForeColor = System.Drawing.Color.White;
            this.textFriendsPort.Location = new System.Drawing.Point(644, 78);
            this.textFriendsPort.Name = "textFriendsPort";
            this.textFriendsPort.Size = new System.Drawing.Size(121, 40);
            this.textFriendsPort.TabIndex = 6;
            // 
            // listMessage
            // 
            this.listMessage.BackColor = System.Drawing.Color.Magenta;
            this.listMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listMessage.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listMessage.ForeColor = System.Drawing.Color.White;
            this.listMessage.FormattingEnabled = true;
            this.listMessage.ItemHeight = 39;
            this.listMessage.Location = new System.Drawing.Point(20, 150);
            this.listMessage.Name = "listMessage";
            this.listMessage.Size = new System.Drawing.Size(768, 156);
            this.listMessage.TabIndex = 8;
            // 
            // textMessage
            // 
            this.textMessage.BackColor = System.Drawing.Color.Magenta;
            this.textMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textMessage.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textMessage.ForeColor = System.Drawing.Color.White;
            this.textMessage.Location = new System.Drawing.Point(20, 354);
            this.textMessage.Name = "textMessage";
            this.textMessage.Size = new System.Drawing.Size(296, 40);
            this.textMessage.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Magenta;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(563, 348);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(225, 50);
            this.button1.TabIndex = 10;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonSend
            // 
            this.buttonSend.BackColor = System.Drawing.Color.Magenta;
            this.buttonSend.FlatAppearance.BorderSize = 0;
            this.buttonSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSend.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSend.ForeColor = System.Drawing.Color.White;
            this.buttonSend.Location = new System.Drawing.Point(322, 348);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(150, 50);
            this.buttonSend.TabIndex = 11;
            this.buttonSend.Text = "Send";
            this.buttonSend.UseVisualStyleBackColor = false;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // topleft
            // 
            this.topleft.Font = new System.Drawing.Font("Comic Sans MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.topleft.Location = new System.Drawing.Point(101, 439);
            this.topleft.Name = "topleft";
            this.topleft.Size = new System.Drawing.Size(120, 120);
            this.topleft.TabIndex = 12;
            this.topleft.Text = "+";
            this.topleft.UseVisualStyleBackColor = true;
            this.topleft.Click += new System.EventHandler(this.topleft_Click);
            // 
            // top
            // 
            this.top.Font = new System.Drawing.Font("Comic Sans MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.top.Location = new System.Drawing.Point(227, 439);
            this.top.Name = "top";
            this.top.Size = new System.Drawing.Size(120, 120);
            this.top.TabIndex = 13;
            this.top.Text = "+";
            this.top.UseVisualStyleBackColor = true;
            this.top.Click += new System.EventHandler(this.top_Click);
            // 
            // topright
            // 
            this.topright.Font = new System.Drawing.Font("Comic Sans MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.topright.Location = new System.Drawing.Point(353, 439);
            this.topright.Name = "topright";
            this.topright.Size = new System.Drawing.Size(120, 120);
            this.topright.TabIndex = 14;
            this.topright.Text = "+";
            this.topright.UseVisualStyleBackColor = true;
            this.topright.Click += new System.EventHandler(this.topright_Click);
            // 
            // left
            // 
            this.left.Font = new System.Drawing.Font("Comic Sans MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.left.Location = new System.Drawing.Point(101, 565);
            this.left.Name = "left";
            this.left.Size = new System.Drawing.Size(120, 120);
            this.left.TabIndex = 15;
            this.left.Text = "+";
            this.left.UseVisualStyleBackColor = true;
            this.left.Click += new System.EventHandler(this.left_Click);
            // 
            // center
            // 
            this.center.Font = new System.Drawing.Font("Comic Sans MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.center.Location = new System.Drawing.Point(227, 565);
            this.center.Name = "center";
            this.center.Size = new System.Drawing.Size(120, 120);
            this.center.TabIndex = 16;
            this.center.Text = "+";
            this.center.UseVisualStyleBackColor = true;
            this.center.Click += new System.EventHandler(this.center_Click);
            // 
            // right
            // 
            this.right.Font = new System.Drawing.Font("Comic Sans MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.right.Location = new System.Drawing.Point(353, 565);
            this.right.Name = "right";
            this.right.Size = new System.Drawing.Size(120, 120);
            this.right.TabIndex = 17;
            this.right.Text = "+";
            this.right.UseVisualStyleBackColor = true;
            this.right.Click += new System.EventHandler(this.right_Click);
            // 
            // bottomleft
            // 
            this.bottomleft.Font = new System.Drawing.Font("Comic Sans MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bottomleft.Location = new System.Drawing.Point(101, 691);
            this.bottomleft.Name = "bottomleft";
            this.bottomleft.Size = new System.Drawing.Size(120, 120);
            this.bottomleft.TabIndex = 18;
            this.bottomleft.Text = "+";
            this.bottomleft.UseVisualStyleBackColor = true;
            this.bottomleft.Click += new System.EventHandler(this.bottomleft_Click);
            // 
            // bottom
            // 
            this.bottom.Font = new System.Drawing.Font("Comic Sans MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bottom.Location = new System.Drawing.Point(227, 691);
            this.bottom.Name = "bottom";
            this.bottom.Size = new System.Drawing.Size(120, 120);
            this.bottom.TabIndex = 19;
            this.bottom.Text = "+";
            this.bottom.UseVisualStyleBackColor = true;
            this.bottom.Click += new System.EventHandler(this.bottom_Click);
            // 
            // bottomright
            // 
            this.bottomright.Font = new System.Drawing.Font("Comic Sans MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bottomright.Location = new System.Drawing.Point(352, 691);
            this.bottomright.Name = "bottomright";
            this.bottomright.Size = new System.Drawing.Size(120, 120);
            this.bottomright.TabIndex = 20;
            this.bottomright.Text = "+";
            this.bottomright.UseVisualStyleBackColor = true;
            this.bottomright.Click += new System.EventHandler(this.bottomright_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(878, 844);
            this.Controls.Add(this.bottomright);
            this.Controls.Add(this.bottom);
            this.Controls.Add(this.bottomleft);
            this.Controls.Add(this.right);
            this.Controls.Add(this.center);
            this.Controls.Add(this.left);
            this.Controls.Add(this.topright);
            this.Controls.Add(this.top);
            this.Controls.Add(this.topleft);
            this.Controls.Add(this.buttonSend);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textMessage);
            this.Controls.Add(this.listMessage);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textFriendsPort);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textFriendIp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textLocalPort);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textLocalIp);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textLocalIp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textLocalPort;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textFriendIp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textFriendsPort;
        private System.Windows.Forms.ListBox listMessage;
        private System.Windows.Forms.TextBox textMessage;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.Button topleft;
        private System.Windows.Forms.Button top;
        private System.Windows.Forms.Button topright;
        private System.Windows.Forms.Button left;
        private System.Windows.Forms.Button center;
        private System.Windows.Forms.Button right;
        private System.Windows.Forms.Button bottomleft;
        private System.Windows.Forms.Button bottom;
        private System.Windows.Forms.Button bottomright;
    }
}

