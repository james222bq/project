﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public int playerHP = 100;
        public int playerDMG = 20;
        //----------------------------
        public int enemyHP = 100;
        public int enemyDMG = 5;
        //----------------------------
        public int wizardHP = 100;
        public int wizardDMG = 5;
        //----------------------------
        public int batHP = 25;
        public int batDMG = 2;
        //----------------------------
        public int ghostHP = 50;
        public int ghostDMG = 5;


        private void button4_Click(object sender, EventArgs e)
        {
            Random random = new Random();

            int enemyMove = random.Next(-15, 15);
            int wizardMove = random.Next(-5, 5);

            if (player.Location.Y < 50)
            {
                MessageBox.Show("Doszedles do konca mapy");
            }
            else
            {
                player.Location = new Point(player.Location.X, player.Location.Y - 25);
                enemy.Location = new Point(enemy.Location.X - enemyMove, enemy.Location.Y - enemyMove);
                wizardp.Location = new Point(wizardp.Location.X - wizardMove, enemy.Location.Y - wizardMove);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random random = new Random();

            int enemyMove2 = random.Next(-15, 15);
            int wizardMove2 = random.Next(-5, 5);

            if (player.Location.X < 60)
            {
                MessageBox.Show("Doszedles do konca mapy");
            }
            else
            {
                player.Location = new Point(player.Location.X - 25, player.Location.Y);
                enemy.Location = new Point(enemy.Location.X - enemyMove2, enemy.Location.Y - enemyMove2);
                wizardp.Location = new Point(wizardp.Location.X - wizardMove2, enemy.Location.Y - wizardMove2);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Random random = new Random();

            int enemyMove3 = random.Next(-15, 15);
            int wizardMove3 = random.Next(-5, 5);

            if (player.Location.X > 900)
            {
                MessageBox.Show("Doszedles do konca mapy");
            }
            else
            {
                player.Location = new Point(player.Location.X + 25, player.Location.Y);
                enemy.Location = new Point(enemy.Location.X + enemyMove3, enemy.Location.Y + enemyMove3);
                wizardp.Location = new Point(wizardp.Location.X + wizardMove3, enemy.Location.Y + wizardMove3);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Random random = new Random();

            int enemyMove4 = random.Next(-15, 15);
            int wizardMove4 = random.Next(-5, 5);

            if (player.Location.Y > 450)
            {
                MessageBox.Show("Doszedles do konca mapy");
            }
            else
            {
                player.Location = new Point(player.Location.X, player.Location.Y + 25);
                enemy.Location = new Point(enemy.Location.X + enemyMove4, enemy.Location.Y + enemyMove4);
                wizardp.Location = new Point(wizardp.Location.X + wizardMove4, enemy.Location.Y + wizardMove4);
            }
        }

        private void attackBtnUp_Click(object sender, EventArgs e)
        {
            Random randomHit = new Random();
            playerhp.Text = playerHP.ToString();
            if (Math.Abs(player.Location.X - enemy.Location.X) < 100 && (Math.Abs(player.Location.Y - enemy.Location.Y) < 100))
            {
                    if (enemyHP == 0)
                    {      
                        enemy.Dispose();
                        wizardp.Visible = true;
                    }
                    else
                    {
                        if(miecz == true)
                        {
                            MessageBox.Show("Uderzenie mieczem");
                        }
                        if(luk == true)
                        {
                            MessageBox.Show("Strzal z luku");
                        }
                        if(maczuga == true)
                        {
                            MessageBox.Show("Uderzenie maczuga");    
                        }
                        if(siekiera == true)
                        {
                            MessageBox.Show("Uderzenie siekiera");
                        }

                        enemyHP -= 20;
                        enemhp.Text = enemyHP.ToString();
                        if (playerHP <= 0)
                            {
                                MessageBox.Show("Przegrales");
                            }
                        else
                           {
                            playerHP -= randomHit.Next(10, 15);
                            playerhp.Text = playerHP.ToString();
                        }

                    }
            }

            if (Math.Abs(player.Location.X - wizardp.Location.X) < 100 && (Math.Abs(player.Location.Y - wizardp.Location.Y) < 100))
            {
                if (wizardHP == 0)
                {
                    wizardp.Dispose();
                }
                else
                {
                    if (miecz == true)
                    {
                        MessageBox.Show("Uderzenie mieczem");
                    }
                    if (luk == true)
                    {
                        MessageBox.Show("Strzal z luku");
                    }
                    if (maczuga == true)
                    {
                        MessageBox.Show("Uderzenie maczuga");
                    }
                    if (siekiera == true)
                    {
                        MessageBox.Show("Uderzenie siekiera");
                    }

                    wizardHP -= 20;
                    wizardhp.Text = wizardHP.ToString();

                    if(playerHP <= 0)
                    {
                        MessageBox.Show("Przegrales");
                    }
                    else
                    {
                        playerHP -= randomHit.Next(2, 10);
                        playerhp.Text = playerHP.ToString();
                    }

                }
            }
            if (enemyHP == 0 && wizardHP == 0)
            {
                wizardp.Dispose();
                MessageBox.Show("WYGRALES");
            }
        }

        private void attackBtnBottom_Click(object sender, EventArgs e)
        {

        }

        private void attackBtnLeft_Click(object sender, EventArgs e)
        {

        }

        private void attackBtnRight_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
        bool miecz;
        bool luk;
        bool maczuga;
        bool siekiera;
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            miecz = true;
            luk = false;
            siekiera = false;
            maczuga = false;
            pictureBox2.BackColor = System.Drawing.Color.Black;
            pictureBox3.BackColor = System.Drawing.Color.White;
            pictureBox4.BackColor = System.Drawing.Color.White;
            pictureBox11.BackColor = System.Drawing.Color.White;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            luk = true;
            miecz = false;
            siekiera = false;
            maczuga = false;
            pictureBox3.BackColor = System.Drawing.Color.Black;
            pictureBox4.BackColor = System.Drawing.Color.White;
            pictureBox11.BackColor = System.Drawing.Color.White;
            pictureBox2.BackColor = System.Drawing.Color.White;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            siekiera = true;
            luk = false;
            miecz = false;
            maczuga = false;
            pictureBox4.BackColor = System.Drawing.Color.Black;
            pictureBox3.BackColor = System.Drawing.Color.White;
            pictureBox11.BackColor = System.Drawing.Color.White;
            pictureBox2.BackColor = System.Drawing.Color.White;
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            maczuga = true;
            miecz = false;
            luk = false;
            siekiera = false;
            pictureBox11.BackColor = System.Drawing.Color.Black;
            pictureBox3.BackColor = System.Drawing.Color.White;
            pictureBox4.BackColor = System.Drawing.Color.White;
            pictureBox2.BackColor = System.Drawing.Color.White;
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            playerHP += 50;
            playerhp.Text = playerHP.ToString();
            pictureBox6.Dispose();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            playerHP += 20;
            playerhp.Text = playerHP.ToString();
            pictureBox7.Dispose();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            enemhp.Text = enemyHP.ToString();
            playerhp.Text = playerHP.ToString();
        }

        private void attackBtnUp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MessageBox.Show("Enter Key Pressed ");
            }
        }
    }
}
