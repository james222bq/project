﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Diagnostics;
using System.IO;
using System.Data.SQLite;

namespace MONITOR
{
    public partial class Form1 : MetroFramework.Forms.MetroForm 
    {
        SQLiteConnection con;
        SQLiteCommand cmd;
        SQLiteDataReader dr;
        bool wyk = false;
        public void CreateDatabaseAndTable()
        {
            if (!File.Exists("MyDatabase.sqlite"))
            {
                SQLiteConnection.CreateFile("MyDatabase.sqlite");
 
                string sql = @"CREATE TABLE dane(
                               ID INTEGER PRIMARY KEY AUTOINCREMENT ,
                               CPU            TEXT      NOT NULL,
                               RAM            TEXT      NOT NULL,
                               DISK           TEXT      NOT NULL
                            );";
                con = new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");
                con.Open();
                cmd = new SQLiteCommand(sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
 
            }
            else
            {
                con = new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");
            }
        }
 
       // private static System.Timers.Timer timer;
        public Form1()
        {
            InitializeComponent();
            /*timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += OntimedEvent;
            timer.AutoReset = true;*/
            CreateDatabaseAndTable();
        }


        public void AddData(string cpu, string ram, string disk)
        {
            cmd = new SQLiteCommand();
            con.Open();
            cmd.Connection = con;
            cmd.CommandText = "insert into dane(CPU,RAM, DISK) values " +
                "('" + cpu + "','" + ram + "', " +
                "'" + disk + "' )";
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public void OntimedEvent(/*Object source, System.Timers.ElapsedEventArgs e*/)
        {
            CheckForIllegalCrossThreadCalls = false;

            //Servicep s = new Servicep();

            /*int cpuValue = s.GetCpuValue();
            int memValue = s.GetMemValue();
            int diskValue = s.GetDiskValue();*/

            //GetDiskValue();

            metroProgressBarCPU.Value = cpuValue;
            metroProgressBarRAM.Value = memValue;
            metroProgressBarDisk.Value = diskValue;

            cpuLabel.Text = metroProgressBarCPU.Value.ToString() + "%";
            ramLabel.Text = metroProgressBarRAM.Value.ToString() + "%";
            diskLabel.Text = metroProgressBarDisk.Value.ToString() + "%";

            string cpu = metroProgressBarCPU.Value.ToString();
            string ram = metroProgressBarRAM.Value.ToString();
            string disk = metroProgressBarDisk.Value.ToString();


            chart1.Series["CPU"].Points.AddY(cpuValue);
            chart1.Series["RAM"].Points.AddY(memValue);
            chart1.Series["DISK"].Points.AddY(diskValue);

            AddData(cpu, ram, disk);

            /*if(metroProgressBarCPU.Value > 75)
            {
                MessageBox.Show("Wysokie uzycie procesora");
            }*/ 
            Process myProcess = new Process();
            
            if (metroProgressBarCPU.Value > 75 && !wyk)
            {
                myProcess.StartInfo.FileName = @"D:\plik.bat";
                //myProcess.StartInfo.CreateNoWindow = false;
                myProcess.Start();
                wyk = true;
            }

        }

       /* private int GetDiskValue()
        {
            var CpuCounter = new PerformanceCounter("PhysicalDisk", "% Disk Time", "_Total");
            CpuCounter.NextValue();
            System.Threading.Thread.Sleep(1000);
            int returnvalue = (int)CpuCounter.NextValue();
            return returnvalue;
        }

        private int GetCpuValue()
        {
            var CpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            CpuCounter.NextValue();
            System.Threading.Thread.Sleep(1000);
            int returnvalue = (int)CpuCounter.NextValue();
            return returnvalue;
        }

        private int GetMemValue()
        {
            /*PerformanceCounter mem = new PerformanceCounter("Memory", "% Committed Bytes In Use");
            float value = mem.NextValue();*/
            /*var MemCounter = new PerformanceCounter("Memory", "% Committed Bytes In Use");           
            System.Threading.Thread.Sleep(1000);
            int returnvalue = (int)MemCounter.NextValue();

            //int value2 = Convert.ToInt32(value);
            //return value2;
            return returnvalue;
        }*/

        private void Form1_Load(object sender, EventArgs e)
        {
            //timer.Enabled = true;
        }

        private void Start_Click(object sender, EventArgs e)
        {
            
        }

        private void metroProgressBar1_Click(object sender, EventArgs e)
        {

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }
    }
}
