﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Numerics;

namespace kalkulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        public string wynik;
        BigInteger nb1 = new BigInteger();
        BigInteger nb2 = new BigInteger();
        BigInteger nb3 = new BigInteger();
        BigInteger nb4 = new BigInteger();
        BigInteger nb5 = new BigInteger();
        BigInteger nb6 = new BigInteger();
        BigInteger nb7 = new BigInteger();
        BigInteger nb8 = new BigInteger();
        BigInteger nb9 = new BigInteger();
        BigInteger nb0 = new BigInteger();
        /*double nb1;
        double nb2;
        double nb3;
        double nb4;
        double nb5;
        double nb6;
        double nb7;
        double nb8;
        double nb9;
        double nb0;*/

        private void buttonN1(object sender, RoutedEventArgs e)
        {
            nb1 = 1;
            wynik = textBox.Text += nb1.ToString();
        }

        private void buttonN2(object sender, RoutedEventArgs e)
        {
            nb2 = 2;
            wynik = textBox.Text += nb2.ToString();
        }

        private void add(object sender, RoutedEventArgs e)
        {
            wynik = textBox.Text += "+";
        }

        private void result(object sender, RoutedEventArgs e)
        {
            /*wynik = textBox.Text;
            MessageBox.Show(wynik.ToString());*/
            var expr = wynik;
            var result = new DataTable().Compute(expr, null);
            //MessageBox.Show(result.ToString());
            textBox.Text = result.ToString();
        }

        private void buttonN3(object sender, RoutedEventArgs e)
        {
            nb3 = 3;
            wynik = textBox.Text += nb3.ToString();
        }

        private void buttonN4(object sender, RoutedEventArgs e)
        {
            nb4 = 4;
            wynik = textBox.Text += nb4.ToString();
        }

        private void buttonN5(object sender, RoutedEventArgs e)
        {
            nb5 = 5;
            wynik = textBox.Text += nb5.ToString();
        }

        private void buttonN6(object sender, RoutedEventArgs e)
        {
            nb6 = 6;
            wynik = textBox.Text += nb6.ToString();
        }

        private void buttonN7(object sender, RoutedEventArgs e)
        {
            nb7 = 7;
            wynik = textBox.Text += nb7.ToString();
        }

        private void buttonN8(object sender, RoutedEventArgs e)
        {
            nb8 = 8;
            wynik = textBox.Text += nb8.ToString();
        }

        private void buttonN9(object sender, RoutedEventArgs e)
        {
            nb9 = 9;
            wynik = textBox.Text += nb9.ToString();
        }

        private void buttonN0(object sender, RoutedEventArgs e)
        {
            nb0 = 0;
            wynik = textBox.Text += nb0.ToString();
        }

        private void nonadd(object sender, RoutedEventArgs e)
        {
            wynik = textBox.Text += "-";
        }

        private void divide(object sender, RoutedEventArgs e)
        {
            wynik = textBox.Text += "/";
        }

        private void multiply(object sender, RoutedEventArgs e)
        {
            wynik = textBox.Text += "*";
        }

        private void N1(object sender, KeyEventArgs e)
        {
            /*if(e.Key == Key.D1)
            {
                wynik = textBox.Text += 1;
            }*/
        }

        private void whichKeyPressed(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                var expr = wynik;
                var result = new DataTable().Compute(expr, null);
                //MessageBox.Show(result.ToString());
                textBox.Text = result.ToString();
            }
        }

        private void minus(object sender, RoutedEventArgs e)
        {

        }
        private void percent(object sender, RoutedEventArgs e)
        {
            var expr = wynik;
            var result = new DataTable().Compute(expr, null);
            //MessageBox.Show(result.ToString());
            string resultt = result.ToString();
            double data = Convert.ToInt32(resultt);
            data *= 0.01;
            textBox.Text = data.ToString();
        }

        private void sqrt(object sender, RoutedEventArgs e)
        {
            var expr = wynik;
            var result = new DataTable().Compute(expr, null);
            string resultp = result.ToString();
            double result0 = Convert.ToInt32(resultp);
            double rp = Math.Sqrt(result0);
            textBox.Text = rp.ToString();
        }

        private void clear(object sender, RoutedEventArgs e)
        {
            textBox.Text = "";
        }

        private void dot(object sender, RoutedEventArgs e)
        {
            wynik = textBox.Text += ".";
        }

        private void getResult(object sender, KeyEventArgs e)
        {
            /*if (e.Key == Key.Enter)
            {
                wynik = textBox.Text;
            }*/
        }
    }
}
