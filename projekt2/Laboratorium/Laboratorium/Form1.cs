﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratorium
{
    public partial class Form1 : Form
    {
        public Greyhound[] GreyhoundArray = new Greyhound[4];
        public Guy[] GuyArray = new Guy[3];
        public Random MyRandomizer = new Random();

        public Form1()
        {
            InitializeComponent();
        }

        public void InitializeGuys()
        {
            GuyArray[0] = new Guy() { Name = "Joe", Cash = 50, MyRadioButton = radioBtn1, MyLabel = joeBetLabel };
            GuyArray[0].UpdateLabels();

            GuyArray[1] = new Guy() { Name = "Bob", Cash = 75, MyRadioButton = radioBtn2, MyLabel = bobBetLabel };
            GuyArray[1].UpdateLabels();

            GuyArray[2] = new Guy() { Name = "Al", Cash = 5, MyRadioButton = radioBtn3, MyLabel = alBetLabel };
            GuyArray[2].UpdateLabels();
        }

        private void dogN1_Click(object sender, EventArgs e)
        {

        }

        private void radioBtn1_CheckedChanged(object sender, EventArgs e)
        {
            nameLabel.Text = "James";
        }

        private void radioBtn2_CheckedChanged(object sender, EventArgs e)
        {
            nameLabel.Text = "xd";
        }

        private void radioBtn3_CheckedChanged(object sender, EventArgs e)
        {
            nameLabel.Text = "222";
        }

        private void betBtn_Click(object sender, EventArgs e)
        {
            if (radioBtn1.Checked)
            {
                if (properValuesInNumericUpDown((int)cashNumericUpDown.Value, (int)dogNumber.Value))
                {
                    GuyArray[0].PlaceBet((int)cashNumericUpDown.Value, (int)dogNumber.Value);
                }
            }
            else if (radioBtn2.Checked)
            {
                if (properValuesInNumericUpDown((int)cashNumericUpDown.Value, (int)dogNumber.Value))
                {
                    GuyArray[1].PlaceBet((int)cashNumericUpDown.Value, (int)dogNumber.Value);
                }
            }
            else if (radioBtn3.Checked)
            {
                if (properValuesInNumericUpDown((int)cashNumericUpDown.Value, (int)dogNumber.Value))
                {
                    GuyArray[2].PlaceBet((int)cashNumericUpDown.Value, (int)dogNumber.Value);
                }
            }
        }

        private void startBtn_Click(object sender, EventArgs e)
        {
            groupBox1.Enabled = false;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < 4; i++)
            {
                if (GreyhoundArray[i].Run())
                {
                    timer1.Stop();
                    MessageBox.Show("Wygrał chart z numerem: " + (i + 1), "Koniec wyścigu");
                    for (int j = 0; j < 4; j++)
                    {
                        GreyhoundArray[j].TakeStartingPosition();
                    }
                    for (int k = 0; k < 3; k++)
                    {
                        GuyArray[k].Collect(i + 1);
                        GuyArray[k].ClearBet();
                        GuyArray[k].UpdateLabels();
                    }
                    groupBox1.Enabled = true;
                }
            }
        }
        
        public void CreatingGreyhounds()
        {
            for (int i = 0; i < 4; i++)
            {
                GreyhoundArray[i] = new Greyhound()
                {
                    StartinPosition = dogN1.Left,
                    RacetrackLength = pictureBox1.Width - dogN1.Width,
                    MyRandom = MyRandomizer
                };
            }

            GreyhoundArray[0].MyPictureBox = dogN1;
            GreyhoundArray[1].MyPictureBox = dogN2;
            GreyhoundArray[2].MyPictureBox = dogN3;
            GreyhoundArray[3].MyPictureBox = dogN4;
        }
        
        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
        public bool properValuesInNumericUpDown(int BetAmount, int GreyhoundNumber)
        {
            if (BetAmount >= 5 && BetAmount <= 15 && GreyhoundNumber >= 1 && GreyhoundNumber <= 4)
                return true;
            else
            {
                MessageBox.Show("Podano nieprawidłową wartość", "Błędna wartość");
                return false;
            }
        }
    }
}
