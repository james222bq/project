﻿
namespace Laboratorium
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.dogN4 = new System.Windows.Forms.PictureBox();
            this.dogN3 = new System.Windows.Forms.PictureBox();
            this.dogN2 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dogNumber = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.cashNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.betBtn = new System.Windows.Forms.Button();
            this.nameLabel = new System.Windows.Forms.Label();
            this.stopBtn = new System.Windows.Forms.Button();
            this.startBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.radioBtn3 = new System.Windows.Forms.RadioButton();
            this.radioBtn2 = new System.Windows.Forms.RadioButton();
            this.radioBtn1 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.joeBetLabel = new System.Windows.Forms.Label();
            this.bobBetLabel = new System.Windows.Forms.Label();
            this.alBetLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dogN1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dogN4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dogN3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dogN2)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dogNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cashNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dogN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Purple;
            this.pictureBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(768, 154);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(200, 125);
            this.pictureBox8.TabIndex = 7;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Purple;
            this.pictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(768, 295);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(200, 125);
            this.pictureBox7.TabIndex = 6;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Purple;
            this.pictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(768, 436);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(200, 125);
            this.pictureBox6.TabIndex = 5;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Purple;
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(768, 12);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(200, 125);
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // dogN4
            // 
            this.dogN4.BackColor = System.Drawing.Color.Yellow;
            this.dogN4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dogN4.Image = ((System.Drawing.Image)(resources.GetObject("dogN4.Image")));
            this.dogN4.Location = new System.Drawing.Point(12, 436);
            this.dogN4.Name = "dogN4";
            this.dogN4.Size = new System.Drawing.Size(125, 125);
            this.dogN4.TabIndex = 3;
            this.dogN4.TabStop = false;
            // 
            // dogN3
            // 
            this.dogN3.BackColor = System.Drawing.Color.Yellow;
            this.dogN3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dogN3.Image = ((System.Drawing.Image)(resources.GetObject("dogN3.Image")));
            this.dogN3.Location = new System.Drawing.Point(12, 295);
            this.dogN3.Name = "dogN3";
            this.dogN3.Size = new System.Drawing.Size(125, 125);
            this.dogN3.TabIndex = 2;
            this.dogN3.TabStop = false;
            // 
            // dogN2
            // 
            this.dogN2.BackColor = System.Drawing.Color.Yellow;
            this.dogN2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dogN2.Image = ((System.Drawing.Image)(resources.GetObject("dogN2.Image")));
            this.dogN2.Location = new System.Drawing.Point(12, 154);
            this.dogN2.Name = "dogN2";
            this.dogN2.Size = new System.Drawing.Size(125, 125);
            this.dogN2.TabIndex = 1;
            this.dogN2.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.alBetLabel);
            this.groupBox1.Controls.Add(this.bobBetLabel);
            this.groupBox1.Controls.Add(this.joeBetLabel);
            this.groupBox1.Controls.Add(this.dogNumber);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cashNumericUpDown);
            this.groupBox1.Controls.Add(this.betBtn);
            this.groupBox1.Controls.Add(this.nameLabel);
            this.groupBox1.Controls.Add(this.stopBtn);
            this.groupBox1.Controls.Add(this.startBtn);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.radioBtn3);
            this.groupBox1.Controls.Add(this.radioBtn2);
            this.groupBox1.Controls.Add(this.radioBtn1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(13, 567);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(955, 276);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DOM BUKMACHERSKI";
            // 
            // dogNumber
            // 
            this.dogNumber.Location = new System.Drawing.Point(564, 207);
            this.dogNumber.Name = "dogNumber";
            this.dogNumber.Size = new System.Drawing.Size(120, 37);
            this.dogNumber.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(435, 211);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 27);
            this.label4.TabIndex = 13;
            this.label4.Text = "zł na psa nr";
            // 
            // cashNumericUpDown
            // 
            this.cashNumericUpDown.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cashNumericUpDown.Location = new System.Drawing.Point(309, 209);
            this.cashNumericUpDown.Name = "cashNumericUpDown";
            this.cashNumericUpDown.Size = new System.Drawing.Size(120, 34);
            this.cashNumericUpDown.TabIndex = 12;
            // 
            // betBtn
            // 
            this.betBtn.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.betBtn.Location = new System.Drawing.Point(188, 200);
            this.betBtn.Name = "betBtn";
            this.betBtn.Size = new System.Drawing.Size(115, 53);
            this.betBtn.TabIndex = 11;
            this.betBtn.Text = "stawiam";
            this.betBtn.UseVisualStyleBackColor = true;
            this.betBtn.Click += new System.EventHandler(this.betBtn_Click);
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nameLabel.Location = new System.Drawing.Point(135, 211);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(42, 27);
            this.nameLabel.TabIndex = 10;
            this.nameLabel.Text = "null";
            // 
            // stopBtn
            // 
            this.stopBtn.BackColor = System.Drawing.Color.Red;
            this.stopBtn.FlatAppearance.BorderSize = 0;
            this.stopBtn.Location = new System.Drawing.Point(740, 153);
            this.stopBtn.Name = "stopBtn";
            this.stopBtn.Size = new System.Drawing.Size(200, 100);
            this.stopBtn.TabIndex = 6;
            this.stopBtn.Text = "STOP";
            this.stopBtn.UseVisualStyleBackColor = false;
            // 
            // startBtn
            // 
            this.startBtn.BackColor = System.Drawing.Color.Magenta;
            this.startBtn.FlatAppearance.BorderSize = 0;
            this.startBtn.Location = new System.Drawing.Point(740, 36);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(200, 100);
            this.startBtn.TabIndex = 5;
            this.startBtn.Text = "START";
            this.startBtn.UseVisualStyleBackColor = false;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(305, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 30);
            this.label2.TabIndex = 4;
            this.label2.Text = "Zakłady:";
            // 
            // radioBtn3
            // 
            this.radioBtn3.AutoSize = true;
            this.radioBtn3.Location = new System.Drawing.Point(12, 153);
            this.radioBtn3.Name = "radioBtn3";
            this.radioBtn3.Size = new System.Drawing.Size(79, 34);
            this.radioBtn3.TabIndex = 3;
            this.radioBtn3.TabStop = true;
            this.radioBtn3.Text = "John";
            this.radioBtn3.UseVisualStyleBackColor = true;
            this.radioBtn3.CheckedChanged += new System.EventHandler(this.radioBtn3_CheckedChanged);
            // 
            // radioBtn2
            // 
            this.radioBtn2.AutoSize = true;
            this.radioBtn2.Location = new System.Drawing.Point(12, 112);
            this.radioBtn2.Name = "radioBtn2";
            this.radioBtn2.Size = new System.Drawing.Size(99, 34);
            this.radioBtn2.TabIndex = 2;
            this.radioBtn2.TabStop = true;
            this.radioBtn2.Text = "Martin";
            this.radioBtn2.UseVisualStyleBackColor = true;
            this.radioBtn2.CheckedChanged += new System.EventHandler(this.radioBtn2_CheckedChanged);
            // 
            // radioBtn1
            // 
            this.radioBtn1.AutoSize = true;
            this.radioBtn1.Location = new System.Drawing.Point(12, 71);
            this.radioBtn1.Name = "radioBtn1";
            this.radioBtn1.Size = new System.Drawing.Size(95, 34);
            this.radioBtn1.TabIndex = 1;
            this.radioBtn1.TabStop = true;
            this.radioBtn1.Text = "James";
            this.radioBtn1.UseVisualStyleBackColor = true;
            this.radioBtn1.CheckedChanged += new System.EventHandler(this.radioBtn1_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(244, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Minimalny zakład: 5 zł";
            // 
            // joeBetLabel
            // 
            this.joeBetLabel.AutoSize = true;
            this.joeBetLabel.Location = new System.Drawing.Point(305, 73);
            this.joeBetLabel.Name = "joeBetLabel";
            this.joeBetLabel.Size = new System.Drawing.Size(74, 30);
            this.joeBetLabel.TabIndex = 15;
            this.joeBetLabel.Text = "label5";
            // 
            // bobBetLabel
            // 
            this.bobBetLabel.AutoSize = true;
            this.bobBetLabel.Location = new System.Drawing.Point(305, 114);
            this.bobBetLabel.Name = "bobBetLabel";
            this.bobBetLabel.Size = new System.Drawing.Size(74, 30);
            this.bobBetLabel.TabIndex = 16;
            this.bobBetLabel.Text = "label6";
            // 
            // alBetLabel
            // 
            this.alBetLabel.AutoSize = true;
            this.alBetLabel.Location = new System.Drawing.Point(305, 155);
            this.alBetLabel.Name = "alBetLabel";
            this.alBetLabel.Size = new System.Drawing.Size(74, 30);
            this.alBetLabel.TabIndex = 17;
            this.alBetLabel.Text = "label7";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // dogN1
            // 
            this.dogN1.BackColor = System.Drawing.Color.Yellow;
            this.dogN1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dogN1.Image = ((System.Drawing.Image)(resources.GetObject("dogN1.Image")));
            this.dogN1.Location = new System.Drawing.Point(13, 12);
            this.dogN1.Name = "dogN1";
            this.dogN1.Size = new System.Drawing.Size(125, 125);
            this.dogN1.TabIndex = 0;
            this.dogN1.TabStop = false;
            this.dogN1.Click += new System.EventHandler(this.dogN1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Yellow;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(956, 549);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1046, 855);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.dogN4);
            this.Controls.Add(this.dogN3);
            this.Controls.Add(this.dogN2);
            this.Controls.Add(this.dogN1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dogN4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dogN3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dogN2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dogNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cashNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dogN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox dogN2;
        private System.Windows.Forms.PictureBox dogN3;
        private System.Windows.Forms.PictureBox dogN4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioBtn3;
        private System.Windows.Forms.RadioButton radioBtn2;
        private System.Windows.Forms.RadioButton radioBtn1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button stopBtn;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown dogNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown cashNumericUpDown;
        private System.Windows.Forms.Button betBtn;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label alBetLabel;
        private System.Windows.Forms.Label bobBetLabel;
        private System.Windows.Forms.Label joeBetLabel;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox dogN1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

