﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorium
{
    public class Bet
    {
        public int Amount;
        public int Dog;
        public Guy Better;

        public string GetDescription()
        {
            if (Amount >= 5 && Amount <= 15)
            {
                return Better.Name + " postawił " + Amount + " zł na psa numer" + Dog;
            }
            else
            {
                return this.Better.Name + " nie zawarł zakładu";
            }
        }

        public int PayOut(int Winner)
        {
            if (Winner == Dog) 
            {
                return Amount;
            }
            else
            {
                return -Amount;
            }
        }
    }
}
