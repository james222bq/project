﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace guy
{
    public partial class Form1 : Form
    {
        Guy james;
        Guy martin;
        long bank = 785687729910;
        public Form1()
        {
            InitializeComponent();

            james = new Guy();
            james.Name = "James";
            james.Cash = 12000;

            martin = new Guy();
            martin.Name = "Martin";
            martin.Cash = 150000;

            Update();
        }

        public void Update()
        {
            jamesLabel.Text = james.Name + " ma " + james.Cash + " zł ";
            martinLabel.Text = martin.Name + " ma " + martin.Cash + " zł ";
            bankLabel.Text = "Bank ma " + james.Cash + " zł ";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(bank >= 10)
            {
                bank -= james.ReceiveCash(10);
                Update();
            }
            else
            {
                MessageBox.Show("Za malo kasy");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bank += martin.GiveCash(5);
            Update();
        }
    }
}
