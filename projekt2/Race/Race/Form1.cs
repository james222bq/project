﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Race
{
    public partial class Form1 : Form
    {
        DinnerParty dinnerParty;
        public Form1()
        {
            InitializeComponent();
            dinnerParty = new DinnerParty()
            {
                NumberOfPeople = 5;
            }
            dinnerParty.setHealthyOption(false);
            dinnerParty.CalculateCostOfDecorations(true);
        }
    }
}
